import { ViewChild } from '@angular/core';
import { Component } from '@angular/core';
import { NgForm } from '@angular/forms';

interface User {
  email: string;
  subscription: string;
  password: string;
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  subscriptionOptions = ['Basic', 'Advanced', 'Pro'];
  defaultSubscription = this.subscriptionOptions[1];
  submitted = false;

  @ViewChild('f', {static: false}) userForm: NgForm;

  user: User = {
    email: '',
    subscription: '',
    password: ''
  };

  onSubmit() {
    console.log(this.userForm);

    this.submitted = true;
    this.user.email = this.userForm.value.email;
    this.user.subscription = this.userForm.value.subscription;
    this.user.password = this.userForm.value.password;

    this.userForm.reset({subscription: this.subscriptionOptions[1]});
  }
}
